document.querySelector("#btn_add_user").addEventListener('click', () => {

	let username = document.querySelector("#username").value;
	let password = document.querySelector("#password").value;
	let confirm = document.querySelector("#confirm").value;
	let firstname = document.querySelector("#firstname").value;
	let lastname = document.querySelector("#lastname").value;
	let address = document.querySelector("#address").value;
	let email = document.querySelector("#email").value;

	// console.log(username, password, confirm, firstname, lastname, address);

	let data = new FormData();

	data.append('username', username);
	data.append('password', password);
	data.append('firstname', firstname);
	data.append('lastname', lastname);
	data.append('address', address);
	data.append('email', email);

	fetch('../controllers/process_create_user.php', {
		method: 'post',
		body: data
	})
		.then(function(response){
			return response.text();
		})
		.then(function(data_from_the_fetched_page){
			console.log(data_from_the_fetched_page);

			if(data_from_the_fetched_page == 'user_exists') {
				document.querySelector("#username").nextElementSibling.innerHTML = 'Username already exists';
			} else {
				window.location.replace('../views/login.php');
			}
		})

})