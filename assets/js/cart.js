// let deleteBtns = document.querySelectorAll(".deleteBtn"); //gets all the delete buttons

// deleteBtns.forEach( function(deleteBtn) {
// 	// deleteBtn.addEventListener("click", ()=> {
// 	// 	deleteBtn.parentElement.submit();
// 	// });

// 	deleteBtn.addEventListener('click', indiv_button => {
// 		alert("hello");
// 		let item_id = indiv_button.target.previousElementSibling.value;
// 		// alert(item_id);

// 		let data = new FormData();
// 		data.append('item_id', item_id);

// 		fetch('../controllers/process_remove_from_cart.php', {
// 			method: 'post',
// 			body: data
// 		})
// 			.then(function(response){
// 				return response.text();
// 			})
// 			.then(function(data_from_fetch){
// 				// console.log(data_from_fetch);
// 				document.querySelector('#cart-table-body')
// 					.innerHTML = data_from_fetch;
// 			})

// 	})
// });

document.addEventListener('click', function(e){
	if(e.target.classList.contains('deleteBtn')){
		let indiv_button = e;
		let item_id = indiv_button.target.previousElementSibling.value;
		// alert(item_id);

		let data = new FormData();
		data.append('item_id', item_id);

		fetch('../controllers/process_remove_from_cart.php', {
			method: 'post',
			body: data
		})
			.then(function(response){
				return response.text();
			})
			.then(function(data_from_fetch){
				// console.log(data_from_fetch);
				if(data_from_fetch != 'empty_cart'){
					document.querySelector('#cart-table-body')
						.innerHTML = data_from_fetch;
				} else {
					window.location.reload(); 	
				}
			})
	}
})



let editInputs = document.querySelectorAll(".quantityInput");
editInputs.forEach( function(editInput){
	editInput.addEventListener("blur", () => {
		editInput.parentElement.submit();
	});
});