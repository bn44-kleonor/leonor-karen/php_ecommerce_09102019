document.querySelector('#btn_login').addEventListener('click', () => {
  let email = document.querySelector('#email').value;
  let password = document.querySelector('#password').value;

  let data = new FormData();

  data.append('email', email);
  data.append('password', password);

  fetch('../controllers/authenticate.php', {
    method: 'post',
    body: data
  })
    .then(function(response) {
      return response.text();
    })
    .then(function(data_from_fetch) {
      // console.log(data_from_fetch);

      if(data_from_fetch == 'login_failed') {
        document.querySelector('.validation').innerHTML = 'Please provide correct login credentials.';
        document.querySelector('.validation').setAttribute('class', 'py-5');
      } else {
        window.location.replace("../views/home.php");
      }
    });
});
