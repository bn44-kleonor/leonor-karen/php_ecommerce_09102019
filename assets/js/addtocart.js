let addtocart_buttons = document.querySelectorAll('.btn-add-to-cart');

addtocart_buttons.forEach(function(addtocart_button) {
  addtocart_button.addEventListener('click', indiv_button => {
    let item_id = indiv_button.target.getAttribute('data-id');
    let item_quantity = indiv_button.target.previousElementSibling.value;

    // alert(item_quantity + " orders of " + item_id + " ordered.");
    if (item_quantity == '' || item_quantity <= 0) {
      alert('Please enter valid quantity');
    } else {
      let data = new FormData();

      data.append('item_id', item_id);
      data.append('item_quantity', item_quantity);

      fetch('../controllers/process_update_cart.php', {
        method: 'post',
        body: data
      })
        .then(function(response) {
          return response.text();
        })
        .then(function(data_from_fetch) {
          console.log(data_from_fetch);
          //TODO: update the badge
          document.querySelector("#cart-count").innerHTML = data_from_fetch;
        });
    }
  });
});
