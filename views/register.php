<?php
	require_once "../partials/template.php";
	function get_content(){
?>
<div class="jumbotron">
	<div class="container">
		<h1 class="display-3">Sign Up</h1>
		<p>This is a sample e-commerce website using native php.</p>
	</div>
</div>

<div class="container">
    <form action="" method="POST">
      <div class="row">

      	<!-- LEFT -->
        <div class="col-lg-6">
          <!-- first name -->
          <div class="form-group">
            <label for="firstname">First Name: </label>
            <input type="text" id="firstname" class="form-control">
            <span class="validation"></span>
          </div>
          <!-- last name -->
          <div class="form-group">
            <label for="lastname">Last Name: </label>
            <input type="text" id="lastname" class="form-control">
            <span class="validation"></span>
          </div>
          <!-- email -->
          <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" id="email" class="form-control">
            <span class="validation"></span>
          </div>
          <!-- address -->
          <div class="form-group">
            <label for="address">Address: </label>
            <input type="text" id="address" class="form-control">
            <span class="validation"></span>
          </div>

        <!-- RIGHT -->
        </div>
        <div class="col-lg-6">
          <!-- username -->
          <div class="form-group">
            <label for="username">Username: </label>
            <input type="text" id="username" class="form-control">
            <span class="validation"></span>
          </div>
          <!-- password -->
          <div class="form-group">
            <label for="password">Password: </label>
            <input type="text" id="password" class="form-control">
            <span class="validation"></span>
          </div>
          <!-- confirm password -->
          <div class="form-group">
            <label for="confirm">Confirm Password: </label>
            <input type="text" id="confirm" class="form-control">
            <span class="validation"></span>
          </div>
          <!-- button -->
		  <div class="form-group my-5">
		     <button type="button" class="btn btn-primary btn-block" id="btn_add_user">Register</button>
		  </div>
        </div>
      </div>
    </form>
</div>

<script type="text/javascript" src="../assets/js/register.js"></script>
<?php } ?>