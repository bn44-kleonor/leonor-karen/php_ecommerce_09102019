<?php
require_once '../partials/template.php';
function get_content(){
    //TO DO: CHECK IF SESSION USER EXISTS; ELSE REDIRECT TO LOGIN
        // TO DO 1: Display recent order of user FROM tbl_orders
        global $conn;
        $order_query = "SELECT * FROM orders WHERE user_id = {$_SESSION['user']['id']} ORDER BY purchase_date DESC LIMIT 1";

        $order_result = mysqli_query($conn, $order_query);
        // var_dump($order_result);
        // var_dump($_SESSION['user']['id']);
        $indiv_order = mysqli_fetch_assoc($order_result);

        //turns assoc array into variables with keys as name
        extract($indiv_order);  
        // var_dump($indiv_order);
        // var_dump($indiv_order['id']);   // w/o extract()
        var_dump($id);                  // with extract()

?>

    <div class="jumbotron">
        <div class="container">
            <!-- TO DO 2: Display transaction code -->
            <h1 class="display-3"><?= $transaction_code ?></h1>
            <p class="mb-5">Here's your transaction code.</p>
            <a href="transactions.php" class="btn btn-primary">View Order History</a>
        </div>
    </div>

    <div class="container mb-5">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="cart-items">
                    <thead>
                        <tr class="text-center">
                            <th>Item</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody id="cart-table-body">

                        <!-- TO DO 3: Display the items of recent order from tbl_items_orders -->
                            <?php 
                                $order_details_query = "SELECT * FROM item_order JOIN items ON (item_order.item_id = items.id) WHERE item_order.order_id = '$id'";
                                // var_dump($order_details_query);

                                $order_details_result = mysqli_query($conn, $order_details_query);
                                // $order_detail = mysqli_fetch_assoc(order_details_result);

                                foreach ($order_details_result as $indiv_item) {
                                    var_dump($indiv_item);
                                    //turn into assoc
                                    //extract
                            ?>
                            <tr>
                                <td><?= $indiv_item['name'] ?></td>
                                <td class="text-center">Php <?= $indiv_item['price'] ?></td>
                                <td class="text-center"><?= $indiv_item['quantity'] ?></td>
                                <td class="text-center">Php <?= $indiv_item['price'] * $indiv_item['quantity'] ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="3" class="text-right">
                                <div class="font-weight-bold">Php <?= $_SESSION['total']?></div>
                            </td>
                            <td class="text-center">
                                <h4 class="font-weight-bold">Total as in</h4>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
<?php } ?>