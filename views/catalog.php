<?php
require_once "../partials/template.php";
function get_content()
{
	global $conn;
	?>
<div class="jumbotron">
	<div class="container">
		<h1 class="display-3">Catalog</h1>
		<p>This is a sample e-commerce website using native php.</p>
	</div>
</div>
<div class="container-fluid mb-5" id="page-catalog">
	<div class="row">
		<div class="col-lg-2">

			<!-- TO DO: RESTRICT ACESS TO BUTTON -->
			<!-- if the user is an admin -->
			<a href="../views/add_item_form.php" class="btn btn-primary btn-block">Add Item</a>

		</div>
		<div class="col-lg-10">
			<!-- display all items as cards -->
			<?php
				$item_query = "SELECT * FROM items";

				$item_list = mysqli_query($conn, $item_query);
				echo "<div class='row'>";

				foreach ($item_list as $item) { ?>
			<div class="col-lg-3 py-2">
				<div class="card h-100">
					<img class="card-img-top" src="<?= $item['image'] ?>">
					<div class="card-body">
						<h4 class="card-title"><?php echo $item['name'] ?></h4>
						<p class="card-text"><?php echo $item['description']; ?></p> <br>
						<p class="card-text">Price: <?php echo $item['price']; ?></p>
					</div>
					<div class="card-footer">

						<!-- TO DO: RESTRICT ACCESS TO BUTTONS -->
						<!-- if the user is an admin -->
						<a href="../views/edit_item_form.php?id=<?= $item['id'] ?>" class="btn btn-primary">Edit Item</a>
						<a href="../controllers/delete_item.php?id=<?= $item['id'] ?>" class="btn btn-danger">Delete Item</a>

						<!-- if the user is not admin or not logged in -->
						<input type="number" class="form-control rounded-0 text-center" value="1" min="1">
						<button type="button" class="btn btn-primary btn-block rounded-0 btn-add-to-cart" data-id='<?= $item['id'] ?>'>
							<i class="fas fa-plus-circle"></i>
							Add to Cart
						</button>

					</div>
				</div>
			</div>


			<?php }
				echo "</div>"; 
				?>

		</div>
	</div>
</div>

<script type="text/javascript" src="../assets/js/addtocart.js"></script>

<?php } ?>