<?php
require_once '../partials/template.php';
function get_content()
{
	var_dump($_SESSION['user']);
	var_dump($_SESSION['user']['role_id'] == 1);
	// condition: if logged in & admin; otherwise redirect to login.php 
	global $conn;
	?>
<!-- Use the $item to populate the contents of the form -->
<div class="jumbotron">
	<div class="container">
		<h1 class="display-3">Add New Item</h1>
		<p>This is a sample e-commerce website using native php.</p>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">

			<form action="../controllers/process_add_item.php" method="POST" enctype="multipart/form-data">

				<input type="hidden" name="id">

				<div class="form-group">
					<label for="name">Item Name: </label>
					<input type="text" name="name" id="name" class="form-control">
				</div>
				<div class="form-group">
					<label for="price">Price: </label>
					<input type="number" name="price" id="price" class="form-control">
				</div>
				<div class="form-group">
					<label for="description">Item Description: </label>
					<textarea id="description" name="description" class="form-control"></textarea>
				</div>
				<!-- CURRENT CODE: name is set to img_path instead of image -->
				<div class="form-group">
					<label for="image">Item Image: </label> <br>
					<input type="file" name="image" id="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Item Category: </label>
					<select id="category_id" name="category_id" class="form-control">
						<?php
							$category_query = "SELECT * FROM categories";
							$category_result = mysqli_query($conn, $category_query);
							foreach ($category_result as $category) { ?>
						<option value="<?= $category['id'] ?>">
							<?= $category['name'] ?>
						</option>
						<?php } ?>

					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-block my-5">Add Item</button>
			</form>
		</div>
	</div>
</div>

<?php } ?>