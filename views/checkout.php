<?php
require_once '../partials/template.php';
function get_content()
{
    // TO DO: CHECK IF USER AND CART SESSIONS EXIST; ELSE, REDIRECT TO LOGIN
   
        global $conn;
        ?>
        
        <!-- TO DO 1: CREATE ORDER SUMMARY MODAL; DECLARE TOTAL AS SESSION HERE -->
         <!-- ORDER SUMMARY MODAL -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">ORDER SUMMARY</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- TABLE -->
                        <div class="container">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="cart-items">
                                        <thead>
                                            <tr class="text-center">
                                                <th></th>
                                                <th>Item</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th class="text-center">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cart-table-body">

                                            <?php
                                                    $total = 0;
                                                    foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
                                                        $sql_query = "SELECT * FROM items WHERE id = $item_id";
                                                        $result = mysqli_query($conn, $sql_query);
                                                        $indiv_item = mysqli_fetch_assoc($result);

                                                        //converts an associative array into a number of variables with names as the keys.
                                                        extract($indiv_item);

                                                        $subtotal = $price * $item_quantity; //$price is the same as $indiv_item['price']
                                                        $total += $subtotal; ?>
                                                <tr id="row<?= $id  ?>">
                                                    <td>
                                                        <input type="hidden" name="id" value="<?= $id ?>">
                                                        <button class="btn btn-flat deleteBtn font-weight-lighter">X</button>
                                                    </td>
                                                    <td>
                                                        <div class="pt-3"><?= $name ?></div>
                                                    </td>
                                                    <td>
                                                        <div class="pt-3">&#8369;<?= $price ?></div>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control text-center" name="item_quantity" value="<?= $item_quantity ?>" class="quantityInput" min="1" disabled>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="pt-3">&#8369; <?= number_format($subtotal, 2); ?></div>
                                                    </td>
                                                </tr>
                                            <?php
                                                    }
                                                    ?>

                                            <tr>
                                                <td colspan="4" class="text-right">
                                                    <div class="font-weight-bold">TOTAL</div>
                                                </td>
                                                <td class="text-center">
                                                    <h4 class="font-weight-bold">&#8369;
                                                        <?php
                                                                $_SESSION['total'] = $total;
                                                                $total = $_SESSION['total'];
                                                                echo number_format($total, 2);
                                                                ?></h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- TO DO 2: CREATE MODAL FOR COD PAYMENT CONFIRMATION -->
        <div class="modal fade" id="cod-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
            Pay Via COD
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Do you want to pay via Cash On Delivery?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-cod">
            YES
        </button>
<!--         <a href="../controllers/process_complete_transaction.php" class="btn btn-primary">
            Yes
        </a> -->
      </div>
    </div>
  </div>
</div>

        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3">Checkout</h1>
                <p>This is a sample e-commerce website using native php.</p>
            </div>
        </div>

        <div class="container mb-5">
            <div class="row">
                <div class="col">
                    <div class="d-flex">
                        <!-- TO DO 1.1: LINK ORDER SUMMARY BUTTON TO MODAL -->
                        <div class="mr-auto">
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-xl">
                                View Order Summary
                            </button>
                        </div>

                        <!-- PAYMENT BUTTON -->
                        <div class="mr-5">
                            <!-- TO DO 2.2: LINK COD BUTTON TO MODAL -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cod-modal">
                                Pay via COD
                            </button>
                        </div>

                        <!-- TO DO 3: RENDER PAYPAL BUTTON -->
                         <div id="paypal-button-container"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- PAYPAL SCRIPTS-->
        <script
    src="https://www.paypal.com/sdk/js?client-id=AYvqkw5HQXrAKtZaJKrU_OylVbs_td9Q4rfzfHjcMo6Vsioio7bve6HpQyTULkBRshnk4kwvTKlFF-RO">
        </script>
        <script>
          paypal.Buttons({
            createOrder: function(data, actions) {
              // Set up the transaction
              return actions.order.create({
                purchase_units: [{
                  amount: {
                    value: "<?= $total ?>"
                  }
                }]
              });
            },
            onApprove: function(data, actions) {
              // Capture the funds from the transaction
              return actions.order.capture().then(function(details) {
                // Show a success message to your buyer
                alert('Transaction completed by ' + details.payer.name.given_name);
                console.log(data);
                console.log(details);

                if(details.status == 'COMPLETED'){
                    alert("completed");
                    let data2 = new FormData();
                    data2.append("orderID", data.orderID);

                    return fetch('../controllers/process_complete_transaction.php', {
                        method: 'post',
                        body: data2
                    });
                    .then(function(response){
                        return response.text();
                    })
                    .then(function(data_from_fetch){
                        console.log(data_from_fetch);
                        //if else 
                        window.location.replace("../views/confirmation.php");
                    })
                }
              });
            }
          }).render('#paypal-button-container');
        </script>
        
        <script type="text/javascript" src="../assets/js/payviacod.js">
            
        </script>

    <?php } ?>

<?php  ?>