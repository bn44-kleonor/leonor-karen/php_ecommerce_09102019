<?php
require_once "../partials/template.php";
function get_content()
{
  ?>
<div class="jumbotron">
  <div class="container">
    <h1 class="display-3">Profile</h1>
    <p>This is a sample e-commerce website using native php.</p>
  </div>
</div>

<div class="container">
  <form action="" method="POST">
    <div class="row">
      <div class="col-lg-6 offset-lg-3">
        <!-- first name -->
        <div class="form-group">
          <label for="firstname">First Name: </label>
          <input type="text" name="firstname" id="firstname" class="form-control" value="<?= $_SESSION['user']['firstname'] ?>">
          <span class="validation"></span>
        </div>
        <!-- last name -->
        <div class="form-group">
          <label for="lastname">Last Name: </label>
          <input type="text" name="lastname" id="lastname" class="form-control" value="<?= $_SESSION['user']['lastname'] ?>">
          <span class="validation"></span>
        </div>
        <!-- email -->
        <div class="form-group">
          <label for="email">Email: </label>
          <input type="email" name="email" id="email" class="form-control" value="<?= $_SESSION['user']['email'] ?>">
          <span class="validation"></span>
        </div>
        <!-- address -->
        <div class="form-group">
          <label for="address">Address: </label>
          <input type="text" name="address" id="address" class="form-control" value="<?= $_SESSION['user']['address'] ?>">
          <span class="validation"></span>
        </div>

      </div>
      <div class="col-lg-6 offset-lg-3">
        <!-- username -->
        <div class="form-group">
          <label for="username">Username: </label>
          <input type="text" name="username" id="username" class="form-control" value="<?= $_SESSION['user']['username'] ?>">
          <span class="validation"></span>
        </div>
        <!-- password -->
        <div class="form-group">
          <label for="password">Password: </label>
          <input type="text" name="password" id="password" class="form-control" value=" ">
          <span class="validation"></span>
        </div>
        <!-- confirm password -->
        <div class="form-group">
          <label for="confirm">Confirm Password: </label>
          <input type="text" name="confirm" id="confirm" class="form-control" value=" ">
          <span class="validation"></span>
        </div>

      </div>
    </div>

    <div class="d-block text-center py-4">
      <button type="submit" class="btn btn-primary btn-block">Edit Profile</button>
    </div>

  </form>
</div>

<?php } ?>