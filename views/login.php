<?php
require_once "../partials/template.php";
function get_content()
{
  ?>
<div class="jumbotron">
  <div class="container">
    <h1 class="display-3">Log In</h1>
    <p>This is a sample e-commerce website using native php.</p>
  </div>
</div>

<div class="container">
  <form action="" method="POST">
    <div class="row">
      <!-- VALIDATION -->
      <div class="col-12 text-center">
        <div class="validation"></div>
      </div>
      <!-- INPUT FIELDS -->
      <div class="col-12 col-lg-6 offset-lg-3">
        <!-- email -->
        <div class="form-group">
          <label for="email">Email: </label>
          <input type="email" id="email" class="form-control">
          <!-- <span class="validation"></span> -->
        </div>

        <!-- password -->
        <div class="form-group">
          <label for="password">Password: </label>
          <input type="text" id="password" class="form-control">
          <!-- <span class="validation"></span> -->
        </div>

        <!-- button -->
        <div class="form-group my-5">
          <button type="button" class="btn btn-primary btn-block" id="btn_login">
            Sign In
          </button>
        </div>
      </div>

    </div>
  </form>
</div>
<script type="text/javascript" src="../assets/js/login.js"></script>
<?php } ?>