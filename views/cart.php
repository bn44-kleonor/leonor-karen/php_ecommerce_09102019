<?php
require_once '../partials/template.php';
function get_content(){
    // TO DO: DISPLAY CART IS EMPTY, IF CART IS EMPTY
    if(isset($_SESSION['cart']) && count($_SESSION['cart']) != 0){
        global $conn;
        $total = 0;
        ?>
        <!-- Use the $item to populate the contents of the form -->
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3">Cart</h1>
                <p>This is a sample e-commerce website using native php.</p>
            </div>
        </div>

        <div class="container mb-5">
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="cart-items">
                        <thead>
                            <tr class="text-center">
                                <th></th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th class="text-center">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody id="cart-table-body">

                            <?php
                                    foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
                                        $sql_query = "SELECT * FROM items WHERE id = $item_id";
                                        $result = mysqli_query($conn, $sql_query);
                                        $indiv_item = mysqli_fetch_assoc($result);

                                        //converts an associative array into a number of variables with names as the keys.
                                        extract($indiv_item);

                                        $subtotal = $price * $item_quantity; //$price is the same as $indiv_item['price']
                                        $total += $subtotal; 
                                ?>
                                <tr id="row<?= $id  ?>">
                                    <td>   
                                            <input type="hidden" name="id" value="<?= $id ?>">
                                            <button class="btn btn-flat deleteBtn">X</button>
                                    </td>
                                    <td>
                                        <div class="pt-3"><?= $name ?></div>
                                    </td>
                                    <td>
                                        <div class="pt-3">Php <?= $price ?></div>
                                    </td>
                                    <td>
                                        <form method="POST" action="../controllers/update_cart.php" id="updateCart">
                                            <input type="hidden" name="item_id" value="<?= $id ?>">
                                            <input type="hidden" name="fromCartPage" value="true">
                                            <input type="number" class="text-center form-control quantityInput" 
                                                name="item_quantity" 
                                                value="<?= $item_quantity ?>" 
                                                min="1">
                                        </form>
                                    </td>
                                    <td class="text-center">
                                        <div class="pt-3">Php <?= number_format($subtotal, 2); ?></div>
                                    </td>

                                </tr>
                            <?php } ?>
                            <tr>
                            
                                <td colspan="4" class="text-right">
                                    <div class="font-weight-bold">TOTAL</div>
                                </td>
                                <td class="text-center">
                                    <h4 class="font-weight-bold">
                                        Php <?= number_format($total, 2) ?>        
                                    </h4>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- TO DO: USE D-FLEX TO PUT BUTTONS ON OPPOSITE ENDS -->
            <div class="row">
                <div class="col">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <!-- TO DO: -->
                            <a href="" class="btn btn-outline-danger">
                                Empty Cart
                            </a>
                        </div>
                        <!-- TO DO 1: LINK TO CHECKOUT.PHP -->
                        <div>
                             <a href="checkout.php" class="btn btn-primary mb-3">
                                 Proceed to checkout
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        
        <!-- PAYPAL -->

    <?php } else { ?>
        <!-- EMPTY CART -->
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3">Empty Cart</h1>
                <p class="mb-5">This is a sample e-commerce website using native php.</p>
                <a href="catalog.php" class="btn btn-primary">
                    Shop Now
                </a>
            </div>
        </div>
    <?php } ?>
    <!-- end of empty cart -->
        

    <script type="text/javascript" src="../assets/js/cart.js"></script>

<?php } ?>
<!-- end of get_content() -->