
<?php
    // var_dump($_SESSION['user']);
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container">
        <a class="navbar-brand" href="index.php"><i class="fab fa-buysellads fa-2x"></i> DEMO Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link" href="catalog.php"><i class="fas fa-book"></i> Catalog
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <!-- TO DO: CHANGE NAV LINKS DEPENDING ON ROLE -->
                <!-- IF LOGGED IN AS ADMIN -->
                <?php if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="">Manage Pages</a>
                </li>
                 <!-- IF LOGGED OUT OR LOGGED IN AND AS NON-ADMIN -->
                <?php } else { ?>
                <li class="nav-item">
                    <a class="nav-link" href="cart.php">
                        <i class="fas fa-shopping-cart"></i>
                        Cart
                        <!-- TO DO: CART COUNT -->
                        <span class="badge bg-light text-dark" id="cart-count">
                            <?php
                                if (isset($_SESSION['cart'])) {
                                    echo array_sum($_SESSION['cart']);
                                    // var_dump($_SESSION['cart']);
                                } else {
                                    echo 0;
                                }
                                ?>
                        </span>
                    </a>
                </li>
                <?php } ?>



                <!-- TO DO: CHANGE NAV-LINKS DEPENDING IF USER IS LOGGED IN OR NOT  -->
                <!-- IF NOT LOGGED IN -->
                <?php if (!isset($_SESSION['user'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="login.php"><i class="fas fa-user-lock"></i> Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php"><i class="fas fa-user-plus"></i> Register</a>
                    </li>
                <?php } else { ?>
                  <!-- IF LOGGED IN -->
                    <li class="nav-item">
                        <a class="nav-link" href="">
                            <i class="fas fa-user-lock"></i>Welcome,  
                            <?php echo $_SESSION['user']['username']; ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
                    </li>
             <?php  } ?>
            </ul>
        </div>
    </div>
</nav>