<?php 

	$host = "localhost";
	$db_username = "root";
	$db_password = "";
	$db_name = "b44_ecommerce_project";

	//Create a connection
	$conn = mysqli_connect($host, $db_username, $db_password, $db_name);

	//check if connection is successful
	if(!$conn){
		die("Connection failed: " . mysqli_error($conn));
	}

?>