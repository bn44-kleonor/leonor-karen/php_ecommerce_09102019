<?php
require_once 'connect.php';

function validate_form()
{
	$errors = 0;
	$file_types = ["jpg", "jpeg", "png", "gif", "svg"];
	$file_ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));

	if(!isset($_POST['name']) || $_POST["name"] == "") {
		$errors++;
	}

	var_dump(!isset($_POST['name']) || $_POST["name"] == "");

	if(!isset($_POST['price']) || $_POST["price"] <= 0) {
		$errors++;
	}

	var_dump(!isset($_POST['price']) || $_POST["price"] <= 0);

	if(!isset($_POST['description']) || $_POST["description"] == "") {
		$errors++;
	}

	var_dump(!isset($_POST['description']) || $_POST["description"] == "");

	if(!isset($_POST['category_id']) || $_POST["category_id"] == "") {
		$errors++;
	}

	var_dump(!isset($_POST['category_id']) || $_POST["category_id"] == "");

	if(!isset($_FILES['image']) || $_FILES["image"] == "") {
		$errors++;
	}

	var_dump(!isset($_FILES['image']) || $_FILES["image"] == "");

	if($errors > 0) {
		return false;
	} else {
		return true;
	}
}

var_dump(validate_form()); //true
// die();

if(validate_form()){
	$id = $_POST['id'];
	$name = $_POST['name'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$image = $_FILES['image'];
	$category_id = $_POST['category_id'];

	if ($image['size'] > 0) {
		$destination = "../assets/images/";

		move_uploaded_file($image['tmp_name'], $destination . $image['name']);
		$whole_file_name = $destination . $image['name'];
	} else {
		$image_query = "SELECT `image` FROM items WHERE id = $id";
		$current_image = mysqli_fetch_assoc(mysqli_query($conn, $image_query));

		$whole_file_name = $current_image['image'];
	}

	$update_query = "UPDATE items SET name = '$name', price = '$price', description = '$description', image = '$whole_file_name', category_id = '$category_id' WHERE id = '$id'";

	$result = mysqli_query($conn, $update_query);

	header("Location: ../views/catalog.php");
} else {
	echo "check input";
}