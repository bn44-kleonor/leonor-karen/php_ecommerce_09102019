<?php
	session_start();
	require "connect.php";
//TO DO 8: if user is logged in, proceed. else, redirect to login
// TO DO 1: generate random transaction code
function generate_transaction_code()
{
	$ref_num = "";
	$source = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];

	for($i = 0; $i < 6; $i++){
		$index = rand(0,15); //any random nums from 0-15
		$ref_num .= $source[$index];
	}
	$today = getdate();

	return $ref_num . "-" . $today[0];
}

//TO DO 2: Prepare values to insert into tbl_orders
    //values: user_id, transaction_code, purchase_date, total, status_id, payment_mode_id
	$user_id = $_SESSION['user']['id'];
	$trans_code = generate_transaction_code();
	$total = $_SESSION['total'];
	if (isset($_POST['orderID'])){
		$status_id = 2;			//completed
		$payment_mode_id = 2;	//paypal
		$paypal_order_id = $_POST['orderID'];
	} else {
		$status_id = 1;			//pending
		$payment_mode_id = 1;	//cod
		$paypal_order_id = NULL;
	}

	//var_dump($trans_code);

//TO DO 3: Insert values into tbl_orders
	$order_query = "INSERT INTO orders (user_id, transaction_code, total, status_id, payment_mode_id, paypal_order_id) VALUES ('$user_id','$trans_code', '$total', '$status_id', '$payment_mode_id', '$paypal_order_id')";
	$order_result = mysqli_query($conn, $order_query);

//TO DO 4: Get order ID of newly created order
	$order_id = mysqli_insert_id($conn); //id of the newly created row

//TO DO 5: Transfer items and their quantity from $_SESSION['cart'] to tbl_items_orders table
	foreach($_SESSION['cart'] as $item_id => $quantity){
		$order_detail_query = "INSERT INTO item_order(order_id, item_id, quantity) VALUES('$order_id', '$item_id', '$quantity')";
		$order_detail_result = mysqli_query($conn, $order_detail_query);
	}
//TO DO 6: unset cart session
	unset($_SESSION['cart']);

//TO DO 7: redirect to confirmation.php
	//header("Location: ../views/confirmation.php");
	echo "transaction_complete";
?>