<?php
	require_once 'connect.php';
	session_start();
	$item_id = $_POST['item_id'];
	unset($_SESSION['cart'][$item_id]);
	$total = 0;

		// header("Location: ". $_SERVER['HTTP_REFERER']);

	// TO DO: ECHO HTML IF CART IS NOT EMPTY, ECHO empty_cart 
	if(array_sum($_SESSION['cart']) > 0){
		foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
	        $sql_query = "SELECT * FROM items WHERE id = $item_id";
	        $result = mysqli_query($conn, $sql_query);
	        $indiv_item = mysqli_fetch_assoc($result);

            //converts an associative array into key-value pairs
            extract($indiv_item);

            $subtotal = $price * $item_quantity; //$price is the same as $indiv_item['price']
            $total += $subtotal; 
?>
          <tr id="row<?= $id  ?>">
                <td>   
                        <input type="hidden" name="id" value="<?= $id ?>">
                        <button class="btn btn-flat deleteBtn">X</button>
                </td>
                <td>
                    <div class="pt-3"><?= $name ?></div>
                </td>
                <td>
                    <div class="pt-3">Php <?= $price ?></div>
                </td>
                <td>
                    <form method="POST" action="../controllers/update_cart.php" id="updateCart">
                        <input type="hidden" name="item_id" value="<?= $id ?>">
                        <input type="hidden" name="fromCartPage" value="true">
                        <input type="number" class="text-center form-control quantityInput" 
                            name="item_quantity" 
                            value="<?= $item_quantity ?>" 
                            min="1">
                    </form>
                </td>
                <td class="text-center">
                    <div class="pt-3">Php <?= number_format($subtotal, 2); ?></div>
                </td>

           </tr>
        <?php } ?>
        <tr> 
            <td colspan="4" class="text-right">
                <div class="font-weight-bold">TOTAL</div>
            </td>
            <td class="text-center">
                <h4 class="font-weight-bold">
                    Php <?= number_format($total, 2) ?>        
                </h4>
            </td>
        </tr>
<?php } else {
	echo "empty_cart";
}
?>